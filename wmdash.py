#!/usr/bin/env python
import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk, Gdk
import sys
import os.path
import toml
import cairo
from optparse import OptionParser
import wmdash_helpers as wml
from wmdash_window import WMDashWindow

def main(argv):
    config = wml.load_config()
    module_paths = wml.get_modules_search_paths()
    module_files = wml.find_modules_with_path_without_ending(module_paths)
    modules = wml.load_modules([*module_files])

    css = wml.get_css()

    cssProvider = Gtk.CssProvider()
    cssProvider.load_from_data(str.encode(css))
    screen = Gdk.Screen.get_default()
    styleContext = Gtk.StyleContext()
    styleContext.add_provider_for_screen(
        screen,
        cssProvider,
        Gtk.STYLE_PROVIDER_PRIORITY_USER
    )

    win = WMDashWindow(config, [*modules])
    win.connect("delete-event", Gtk.main_quit)
    win.connect("destroy", Gtk.main_quit)
    win.show_all()
    Gtk.main()


if __name__ == "__main__":

    parser = OptionParser()
    parser.add_option(
        "--dump-css",
        dest="dumpcss",
        action="store_true",
        help="dump the assembled CSS to stdout"
    )

    parser.add_option(
        "--dump-config",
        dest="dumpconf",
        action="store_true",
        help="dump the assembled config (including user conf) to stdout"
    )

    (options, args) = parser.parse_args()

    if options.dumpcss:
        print(wml.get_css())
    if options.dumpconf:
        wml.dumpconf()
    else:
        main(sys.argv)
