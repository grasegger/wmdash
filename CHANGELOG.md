# Changelog

## 0.3.0

- new api for widgets
- each widget is now an instance with own config
- cheatsheet got a new config
- battery time formatting fixed
- all XDG_CONFIG dirs are searched now
- only last found config is used
- script module available
- image module available

## 0.2.0

### core

#### Widget API

##### Timeout

The timeout of a module is now read using the function "get_timeout" with the app configuration as an argument. Timeout fields are now ignored.

#### Arguments

The app now uses an optionparser to enable command line arguments to be read in a convinient fashion.

#### Command Line

##### Help

The "--help" option displays a short manual of the application.

##### Dump CSS

The "--dump-css" option assembles all css found and writes it to STDOUT.

##### Dump Config

The "--dump-config" option loads the config (inlcluding user config) and writes it to STDOUT.

#### configuration

#### General
Configuration is now done in toml.

#### Modified Config Options

##### Pane Order

The option was renamed from "wmdash.order" to "wmdash.paneorder".

Only panes mention in this array will display. The order will be first to last from left to right.

Possible array items are "static"

#### New Config Options

##### Module Timeouts

The shipped modules now use the config to set timeouts for updates. The timeout is in miliseconds.

```toml
[time]
timeout = 1000

[battery]
timeout = 60000

[disks]
timeout = 60000

[date]
timeout = 1200000
```

##### Window Placement

If the wmdash window is not fullscreen it tries to move to the coordinates configured.

The following section controls the coordinates (from top left of the screen):

```toml
[wmdash.window]
x = 100
y = 100
```

##### Keyboard Shortcuts

It is now possible to define keyboard shortcuts for actions.

To add a key binding you can take the following snippet and tweak it, see below for the explanation of possible values.

```toml
[[keys]]
key.target = ""
key.action = ""
key.key = ""
key.modifiers = []
```

###### ```key.target```

Tell wmdash what part part of the programm you are binding the key to. This should be either ```none```, ```wmdash``` or the name of a plugin. You can use ```none``` to unbind keys.

Please notice that plugin key bindings only work if a plugin has the focus.

###### ```key.action```

Define the action the key combo should trigger. wmdash has the actions ```quit``` and ```fullscreen```.

###### ```key.key```

The symbol which triggers the action. Please note that wmdash does not care about the case of letters and if you want to use an uppercase letter you have to add ```shift``` to the modifiers (see below).

###### ```key.modifiers```

This is an array of modifiers which are needed to trigger an action. Possible values are:

- ctrl
- alt
- super
- shift

#### Default Key Bindings

- ```q```: target = wmdash; action = quit
- ```f```: target = wmdash; action = fullscreen

#### Default Window Placement

The default placement for non-fullscreen windows is a 100px from the top and a 100px from the left of the display.

### Widget API

#### config parameter

The ```config``` parameter now is an object and not a dict anymore.

## 0.1.0

### core

#### Pane ordering

The order of the 3 main panes can now be changed.

```yml
wmdash:
    order:
    - static
    - dynamic
    - notifications
```

The order list needs to have exactly 3 elements. If a pane is mispelled it wont be loaded it, but don't use this as a feature because there is already an issue for config options to disable panes.

#### Module blacklisting

You can now disable certain modules.

```yml
wmdash:
    blacklist-modules:
    - battery
```

The options takes a list of module names (the name of the module is the filename without ```.py``` at the end)


#### Widget API

##### [new] is_disabled
If a module exposes a ```is_disabled``` function the result will be honored.

The module cant override the user blacklist configuration.
The user blacklist cant override the module configuration.

This feature is for modules that depend on a specific system state to function properly, for example a battery module can disable itself if it loaded on a system that has no battery attached.

##### [changed] getWidget

If an .glade file is found alongside the .py-file of any module wmdash will call ```module.getWidget(config, uifile)``` instead of ```module.getWidget(config)```. This enables widget to prototype or boilerplate in the glade editor and easily make a module out of it.

##### [new] update

If a module has a timeout propterty and a update function the update function is called in the interval timeout specified.
The udpate function is called with a list as an argument. The first element of the list is the widget the module exposes via its ```getWidget``` function, the second argument is the configuration.

### widgets

#### battery

The battery module now disables itsef if no battery is detected.
The battery module now updates every two minutes.

#### date

There is now a date module available.
The date module now updates every minute.

#### disks

The disks module now updates itself every minute.

#### time

The time module now updates itself every second.
