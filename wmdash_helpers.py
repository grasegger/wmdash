import gi

gi.require_version ('Gtk', '3.0')
gi.require_version ('Gdk', '3.0')

from gi.repository import Gtk, Gdk

import sys
import re
import os.path
from xdg import XDG_CONFIG_HOME, XDG_DATA_DIRS, XDG_DATA_HOME, XDG_CONFIG_DIRS
import toml
from pathlib import PosixPath
from typing import List
from deepmerge import Merger
import importlib
from box import Box
import inspect
from wmdash_widget import WMDashWidget, WidgetAPIDefunct

def getWidgetClass(module):
    for name, obj in inspect.getmembers(module):
        if inspect.isclass(obj) and issubclass(obj, WMDashWidget):
            return obj

    # If nothing is returned at this point the widget is not implemented corretcly
    raise WidgetAPIDefunct(f"The module {module.__name__} does not contain a subclass of WMDashWidget")

def assemble_css(css_files):
    css = ""
    for f in css_files:
        with open(f) as cssfile:
            css = css + cssfile.read()

    return css


def remove_file_ending(path: str) -> str:
    return re.sub(r'\.[^\.]*$', '', path)


def get_app_dir_modules_path() -> PosixPath:
    location = os.path.dirname(sys.argv[0])
    absolute_location = os.path.abspath(location)

    return PosixPath(absolute_location)


def get_modules_search_paths() -> List[PosixPath]:
    locations = XDG_DATA_DIRS
    locations.append(XDG_DATA_HOME)
    wmdash_dirs = [*map(lambda l: l.joinpath('wmdash'), locations)]

    wmdash_dirs.append(get_app_dir_modules_path())
    module_dirs = [*map(lambda l: l.joinpath('modules'), wmdash_dirs)]

    return [*filter(lambda l: os.path.isdir(l), module_dirs)]


def find_modules_with_path_without_ending(paths: List[PosixPath]):
    for d in paths:
        files = os.scandir(d)
        matches = filter(lambda f: f.name.endswith('.py'), files)
        for f in matches:
            yield remove_file_ending(f.path)


def load_config():
    app_config_path = os.path.abspath(os.path.dirname(sys.argv[0])) + '/config.toml'

    config = toml.load(app_config_path)

    for location in XDG_CONFIG_DIRS:
        try:
            conf_location = location.joinpath('wmdash', 'config.toml')
            config = toml.load(conf_location)
        except FileNotFoundError:
            pass

    try:
        conf_location = XDG_CONFIG_HOME.joinpath('wmdash','config.toml')
        config = toml.load(conf_location)
    except FileNotFoundError:
        pass

    return Box(config)


def load_modules(locations):
    for l in locations:
        module_name = "wmdash_" + os.path.basename(l)
        spec = importlib.util.spec_from_file_location(module_name, f'{l}.py')
        newmodule = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(newmodule)
        yield newmodule


def get_css_paths(modules):
    app_css = os.path.abspath(os.path.dirname(sys.argv[0])) + '/style.css'

    if os.path.exists(app_css):
        yield app_css

    for m in modules:
        csspath = f'{m}.css'
        if os.path.exists(csspath):
            yield csspath

    user_css = XDG_CONFIG_HOME.joinpath('wmdash','wmdash.css')
    if os.path.exists(user_css):
        yield user_css
def get_css():
    module_paths = get_modules_search_paths()
    module_files = find_modules_with_path_without_ending(module_paths)
    css_files = get_css_paths([*module_files])

    css = assemble_css(css_files)

    return css


def dumpconf():
    config = load_config()

    document = toml.dumps(config)
    print(config)
    print(document)
