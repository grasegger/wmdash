# wmdash

... because panels are boring

## panes

the layout of wmdash consists of 3 panes:

- static (not implemented)
- dynamic
- notifications (not implemented)

a forth region for a potential search function will be placed above or below those panes in the future I guess.
