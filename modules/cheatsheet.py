import gi

gi.require_version( 'Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
import datetime
from wmdash_widget import WMDashWidget, WMDashWidgetType, WidgetDisabledException


class Cheatsheet(WMDashWidget):
    type = WMDashWidgetType.STATIC

    def __init__(self, config, gladefile):
        super(Cheatsheet, self).__init__(config)
        try:
            self._widget = self.get_app_wrapper()
        except KeyError:
            raise WidgetDisabledException(self.name, "Configuration seems to be faulty.")

    def get_app_wrapper(self):
        wrapper = Gtk.VBox(homogeneous=True, spacing=0)
        wrapper.set_halign(Gtk.Align.CENTER)
        wrapper.set_valign(Gtk.Align.CENTER)

        app_label = Gtk.Label(label=self.config.application)
        app_label.set_valign(Gtk.Align.CENTER)
        app_label.get_style_context().add_class('cheatsheet-app-label')

        wrapper.pack_start(app_label, False, False, 0)

        for bind in self.get_binding_widgets():
            wrapper.pack_start(bind, False, False, 0)

        return wrapper

    def get_modifier_label(self, mod):
        label = Gtk.Label(label=mod)

        label_style_context = label.get_style_context()
        label_style_context.add_class('key-modifier')

        return label

    def get_key_label(self, key):
        label = Gtk.Label(label=key)

        label_style_context = label.get_style_context()
        label_style_context.add_class('key')

        return label

    def get_binding_widgets(self):
        for combo in self.config.combos:
            combo_container = Gtk.HBox(homogeneous=True, spacing=0)

            for sequence in combo.sequence.split(' '):
                sequence_box = Gtk.HBox(homogeneous=True, spacing=0)

                sequence_box_style_context = sequence_box.get_style_context()
                sequence_box_style_context.add_class("key-sequence")

                modifiers = sequence.split('-')[:-1]
                key = sequence.split('-')[-1]

                for mod in modifiers:
                    mod_label = self.get_modifier_label(mod)
                    sequence_box.pack_start(mod_label, False, False, 0)

                key_label = self.get_key_label(key)
                sequence_box.pack_start(key_label, False, False, 0)

                combo_container.pack_start(sequence_box, False, False, 0)

            action_label = Gtk.Label(label=combo.action)
            combo_container.pack_start(action_label, False, False, 0)

            yield combo_container
