import gi

gi.require_version( 'Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
import datetime
from wmdash_widget import WMDashWidget, WMDashWidgetType


class Date(WMDashWidget):
    type = WMDashWidgetType.DYNAMIC

    def __init__(self, config, gladefile):
        timeout = 1000 * 60 # 1 Minute
        super(Date, self).__init__(config, gladefile, timeout)

        # since this widget uses an ui file we can get it this way
        self._widget = self.builder.get_object('widget')

        self.update()

    def update(self):
        label = self._widget.get_children()[0]
        now = datetime.datetime.now()
        label.set_label(now.strftime("%Y-%m-%d"))

        return True
