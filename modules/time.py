import gi

gi.require_version( 'Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
import datetime
from wmdash_widget import WMDashWidget, WMDashWidgetType, WidgetDisabledException


class Time(WMDashWidget):
    type = WMDashWidgetType.DYNAMIC

    def __init__(self, config, gladefile):
        timeout = 1000  # 1 second
        super(Time, self).__init__(config, gladefile, timeout)

        self._widget = Gtk.Label(label="Loading ...")
        self.update()

    def update(self):
        now = datetime.datetime.now()
        self._widget.set_label(now.strftime("%H:%M:%S"))

        return True
