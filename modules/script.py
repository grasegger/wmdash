import gi

gi.require_version( 'Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
from wmdash_widget import WMDashWidget, WMDashWidgetType, WidgetDisabledException
from subprocess import run


class Script(WMDashWidget):
    def __init__(self, config, gladefile):
        timeout = 1000  # default 1 minute

        try:
            self.command = config.command
        except KeyError:
            raise WidgetDisabledException(self.name, "The script widget needs a command option")

        try:
            self.get_command_output()
        except Exception as e:
            raise WidgetDisabledException(self.name, e)
            
        super(Script, self).__init__(config, gladefile, timeout)

        self._widget = Gtk.Label(label="Loading ...")
        self.update()

    def get_command_output(self):
        args = self.command.split()
        result = run(args, capture_output=True, check=True)
        return result.stdout.decode()

    def update(self):
        self._widget.set_label(self.get_command_output())

        return True

    @property
    def type(self):
        try:
            if self.config.type == "static":
                return WMDashWidgetType.STATIC
            elif self.config.type == "dynamic":
                return WMDashWidgetType.DYNAMIC
            else:
                raise WidgetDisabledException(self.name, "Type must be one of 'static' or 'dynamic'")
        except KeyError:
            raise WidgetDisabledException(self.name, "Script must have a type field in config.")

