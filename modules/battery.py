import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
import psutil
import datetime
from wmdash_widget import WMDashWidget, WMDashWidgetType, WidgetDisabledException


class Battery(WMDashWidget):
    type = WMDashWidgetType.DYNAMIC

    def __init__(self, config, gladefile):
        timeout = 2000 * 60  # default 2 minutes
        super(Battery, self).__init__(config, gladefile, timeout)

        self._widget = Gtk.Label(label="Battery data loading ...")

        self.update()

    def update(self) -> bool:
        data = psutil.sensors_battery()

        if data is None:
            raise(WidgetDisabledException(self.name, "No batteries found."))

        percent = int(data.percent)
        state = ""

        if not data.power_plugged:
            hours = int (data.secsleft / 3600)
            minutes = int((data.secsleft - hours * 3600) / 60)
            state = f"{hours}:{minutes:02} remaining"
        else:
            state = "charging"

        self._widget.set_label(f"{percent}% - {state}")

        return True
