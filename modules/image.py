import gi

gi.require_version( 'Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
from wmdash_widget import WMDashWidget, WMDashWidgetType, WidgetDisabledException
from os.path import exists

class Image(WMDashWidget):
    type = WMDashWidgetType.STATIC
    
    def __init__(self, config, gladefile):
        super(Image, self).__init__(config)

                
        try:
            if not exists(config.path):
                raise WidgetDisabledException(self.name, f"{config.path} does not exist.")

            self._widget = Gtk.Image.new_from_file(config.path)
        except KeyError:
            raise WidgetDisabledException(self.name, "Configuration needs a path.")

