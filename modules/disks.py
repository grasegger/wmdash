import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk
import psutil
from wmdash_widget import WMDashWidget, WMDashWidgetType, WidgetDisabledException
from math import ceil


class Disks(WMDashWidget):
    type = WMDashWidgetType.DYNAMIC

    def __init__(self, config, uipath):
        timeout = 2000 #* 60  # 2 minutes

        try:
            self.mountpoint = config.mountpoint
        except KeyError:
            raise WidgetDisabledException(self.name, "Missing config parameter 'mountpoint'")

        try:
            next(filter(lambda d: d.mountpoint == self.mountpoint , psutil.disk_partitions()))
        except StopIteration:
            raise WidgetDisabledException(self.name, f"Mountpoint {self.mountpoint} not found.")

        super(Disks, self).__init__(config, uipath, timeout)

        self._widget = Gtk.HBox(homogeneous=True, spacing=0, name="disks")
        self._widget.set_halign(Gtk.Align.FILL)
        self._widget.set_valign(Gtk.Align.FILL)

        self.fill_widget()

    def fill_widget(self):
        free = self.get_disk_with_free_space()

        mountlabel = Gtk.Label(label=self.mountpoint)
        spacelabel = Gtk.Label(label=f'{free}% free')
        spacelabel.set_name("free")

        self._widget.pack_start(mountlabel, False, False, 0)
        self._widget.pack_start(spacelabel, False, False, 0)

    def get_disk_with_free_space(self):
        return 100 - ceil(psutil.disk_usage(self.mountpoint).percent)

    def update(self):
        free = self.get_disk_with_free_space()

        for child in self._widget.get_children():
            if 'space' in child.get_name():
                child.set_label(f"{free}% free")

        return True
