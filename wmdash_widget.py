import gi

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, GLib, Gdk
from box import Box
from enum import Enum
from os.path import exists


class WMDashWidgetType(Enum):
    STATIC = 1
    DYNAMIC = 2
    NOTIFICATIONS = 3


class WidgetDisabledException(Exception):
    """
    Exception raised when the module is disabled.
    """
    def __init__(self, module, reason):
        self.message = f'[modules] {module} is disabled: "{reason}"'

    def __str__(self):
        return self.message


class WidgetAPIDefunct(Exception):
    """
    Exception raised then the widget does not implement the API corretcly
    """
    def __init__(self, reason):
        self.message = reason


class WMDashWidget(object):
    """
    WMDash Widget

    This is an abstract class for widget to base upon. It defines the api for providing a widget.
    """
    def __init__(self, config: Box, gladefile: str = "", timeout=1000):
        super(WMDashWidget, self).__init__()

        try:
            if config.disabled:
                raise WidgetDisabledException(self.name, "Config disabled the widget")
        except WidgetDisabledException as e:
            raise(e)
        except KeyError:
            pass  # Widget is not disabled.
        except Exception as e:
            raise(e)

        try:
            self._name = config.name
        except KeyError:
            pass

        self.config = config
        try:
            self._builder = Gtk.Builder()
            self._builder.add_from_file(gladefile)
        except GLib.Error as e:
            if e.matches(GLib.file_error_quark(), e.code):
                pass
                self._builder = None
            else:
                raise(e)
        except Exception as e:
            raise (e)

        self.timeout = timeout

        try:
            self.timeout = self.config.timeout
        except Exception:
            pass

        if self.type != WMDashWidgetType.STATIC:
            Gdk.threads_add_timeout(0, self.timeout, self.update)

    @property
    def builder(self) -> Gtk.Builder:
        return self._builder

    @property
    def type(self) -> WMDashWidgetType:
        raise WidgetAPIDefunct(f"The widget {self.name} did not define the type property.")

    @property
    def widget(self) -> Gtk.Widget:
        try:
            widget_style_context = self._widget.get_style_context()

            if not widget_style_context.has_class(self.name):
                widget_style_context.add_class(self.name)

            if not widget_style_context.has_class(self.__class__.__name__.lower()):
                widget_style_context.add_class(self.__class__.__name__.lower())

            return self._widget
        except AttributeError:
            raise WidgetAPIDefunct(f"The widget {self.name} has no _widget property.")
        except Exception as e:
            raise(e)

    def update(self) -> bool:
        raise WidgetAPIDefunct(f"The widget {self.name} did not implement the update method.")


    @property
    def name(self):
        try:
            return self._name
        except AttributeError:
            return self.__class__.__name__.lower()
