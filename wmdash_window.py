import gi

gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')

from gi.repository import Gtk, Gdk
import cairo
import os
import wmdash_helpers as wml
import inspect
import wmdash_widget as wmw

class WMDashWindow(Gtk.Window):
    def get_pane(self, panename):
        pane = Gtk.VBox()
        pane.set_name(panename)
        pane.set_halign(Gtk.Align.FILL)
        pane.set_valign(Gtk.Align.FILL)

        type_filter = wmw.WMDashWidgetType[panename.upper()]

        for module in filter(lambda x: x.type == type_filter, self.modules):
            pane.pack_start(module.widget, True, True, 0)

        return pane

    def __init__(self, config, modules):
        self.config = config
        self.isfullscreen = config.wmdash.window.fullscreen

        Gtk.Window.__init__(self, title=self.config.wmdash.window.name)
        self.set_name(self.config.wmdash.window.name)
        self.set_default_size(
            self.config.wmdash.window.width,
            self.config.wmdash.window.height
            )
        if config.wmdash.window.fullscreen:
            self.fullscreen()
        else:
            self.move_to_target_coordinates()

        self.modules = []

        try:
            for widget in self.config.widgets:
                try:
                    modulename = f"wmdash_{widget.module}"
                    module = next(filter(lambda m: m.__name__ == modulename, modules))

                    widget_class = wml.getWidgetClass(module)
                    uifile_path = module.__file__.replace('.py', '.glade')
                    self.modules.append(widget_class(widget, uifile_path))
                except StopIteration:
                    print(f"The configuration tried to the module {modulename}, which is not present.")
                except wmw.WidgetDisabledException as e:
                    # If a widget is disabled we discard this error and continue.
                    print(e)
                except wmw.WidgetAPIDefunct as e:
                    print(e)
        except KeyError as e:
            print("It seems as if there are no modules defined in your config. Use the --dump-config parameter see the current configuration.")
        except Exception as e:
            raise(e)

        self.box = Gtk.HBox(homogeneous=True, spacing=0)
        self.box.set_halign(Gtk.Align.FILL)
        self.box.set_valign(Gtk.Align.FILL)
        self.box.set_name('wrapper')

        self.add(self.box)

        for pane in self.config['wmdash']['paneorder']:
            widget = self.get_pane(pane)
            self.box.pack_start(widget, True, True, 0)

        # connect the key-press event - this will call the keypress
        # handler when any key is pressed
        self.connect(
            "key-press-event",
            self.on_key_press_event
        )

        screen = self.get_screen()
        visual = screen.get_rgba_visual()
        if visual and screen.is_composited():
            self.set_visual(visual)

        self.set_app_paintable(True)

        self.connect('draw', self.draw)

    def draw(self, widget, context):
        context.set_source_rgba(0, 0, 0, 0)
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.set_operator(cairo.OPERATOR_OVER)

    def move_to_target_coordinates(self):
        self.move(self.config.wmdash.window.x, self.config.wmdash.window.y)

    def on_key_press_event(self, widget, event):
        action = self.check_for_key_mapping(event)

        if action['target'] == 'none':
            pass
        elif action['target'] == 'wmdash':
            self.handle_wmdash_keycombo(action['action'])
        else:
            self.handle_plugin_keycombo(action['target'], action['action'])

    def toggle_fullscreen(self):
        if self.isfullscreen:
            self.isfullscreen = not self.isfullscreen
            self.unfullscreen()
            self.move_to_target_coordinates()
        else:
            self.isfullscreen = not self.isfullscreen
            self.fullscreen()

    def refresh_wmdash(self):
        pass

    def handle_wmdash_keycombo(self, action):
        if action == "quit":
            self.destroy()
        elif action == "fullscreen":
            self.toggle_fullscreen()
        else:
            print(f"Unknown action: {action} for wmdash")

    def handle_plugin_keycombo(self, target, action):
        print("plugin key combos are not yet implemented")

    def check_for_modifier(self, modifier, searchlist, pressed_state):
        return list(filter(lambda k:
                           (modifier in k.key.modifiers) == pressed_state,
                           searchlist))

    def check_for_key_mapping(self, event):
        result = {'target': 'none', 'action': 'none'}

        key = Gdk.keyval_name(event.keyval).lower()
        ctrl_pressed = bool(event.state & Gdk.ModifierType.CONTROL_MASK)
        shift_pressed = bool(event.state & Gdk.ModifierType.SHIFT_MASK)
        super_pressed = bool(event.state & Gdk.ModifierType.SUPER_MASK)
        alt_pressed = bool(event.state & Gdk.ModifierType.MOD1_MASK)

        matches = list(filter(lambda k: k.key.key == key, self.config['keys']))
        matches = self.check_for_modifier('ctrl', matches, ctrl_pressed)
        matches = self.check_for_modifier('shift', matches, shift_pressed)
        matches = self.check_for_modifier('super', matches, super_pressed)
        matches = self.check_for_modifier('alt', matches, alt_pressed)

        if len(matches) > 0:
            bestmatch = matches[-1]

            result['target'] = bestmatch.key.target
            result['action'] = bestmatch.key.action

        return result
