#!/usr/bin/env python3
import unittest
import wmdash

class TestWMDash(unittest.TestCase):

    def test_remove_file_ending(self):
        self.assertEqual(wmdash.remove_file_ending('test.py'), 'test')
        self.assertEqual(wmdash.remove_file_ending('test.py.txt'), 'test.py')

if __name__ == '__main__':
    unittest.main()
